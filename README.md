[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

## Starter kit

The purpose of this repository is to start a project with a minimal configuration on javascript files, formatting and test coverage.

* [Airbnb configuration](https://www.npmjs.com/package/eslint-config-airbnb)
* [Prettier](https://github.com/prettier/prettier)
* [Jest](https://www.npmjs.com/package/jest)
* [Commit hooks](https://github.com/brigade/overcommit)
