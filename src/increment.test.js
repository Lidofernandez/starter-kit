const incrementValue = require("./increment");

describe("Increment", () => {
  it("should increment the value plus one", () => {
    expect(incrementValue()).toEqual(1);
    expect(incrementValue(1)).toEqual(2);
  });
});
