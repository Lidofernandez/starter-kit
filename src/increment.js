function incrementValue(value = 0) {
  let totalValue = value;
  totalValue += 1;
  return totalValue;
}

module.exports = incrementValue;
