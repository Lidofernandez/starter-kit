module Overcommit::Hook::PrePush
  # Prevents pushes to specified branches.
  # Based on ProtectedBranches
  class DisabledBranches < Base
    def run
      return :pass unless illegal_pushes.any?

      messages = illegal_pushes.map do |pushed_ref|
        "Pushing to #{pushed_ref.remote_ref} is not allowed."
      end

      [:fail, messages.join("\n")]
    end

    private

    def illegal_pushes
      @illegal_pushes ||= pushed_refs.select do |pushed_ref|
        disabled? pushed_ref.remote_ref
      end
    end

    def disabled?(remote_ref)
      ref_name = remote_ref[%r{refs/heads/(.*)}, 1]
      disabled_branch_patterns.any? do |pattern|
        File.fnmatch(pattern, ref_name)
      end
    end

    def disabled_branch_patterns
      @disabled_branch_patterns ||= Array(config['branches']).
        concat(Array(config['branch_patterns']))
    end
  end
end
