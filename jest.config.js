module.exports = {
  setupFiles: ["<rootDir>/jest.setup.js"],
  collectCoverageFrom: ["<rootDir>/src/**/*"],
  coverageThreshold: {
    global: {
      branches: 60,
      functions: 60,
      lines: 60,
      statements: 60,
    },
  },
};
